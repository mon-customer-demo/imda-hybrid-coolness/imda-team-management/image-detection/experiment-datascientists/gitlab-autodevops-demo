package object;

public class MyObject {

	private double valeur;

	public MyObject(double a) {
		this.valeur = a;
		System.out.println("la valeur actuelle est : " + this.valeur);
	};
	
	public double add(double a) {
		this.valeur = this.valeur + a;
		System.out.println("l'addition donne : " + this.valeur);
		return this.valeur;	
	}
	
	public double substract(double a) {
		this.valeur = this.valeur - a;
		System.out.println("la soustraction donne : " + this.valeur);
		return this.valeur;		
	}
	
	public String print() {
		System.out.println("la valeur actuelle est : " +this.getResult());
		return "la valeur actuelle est : " + this.getResult();
	}

	public double getResult() {
		return valeur;
	}
}